package epam.com.post.domain.user;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Company {
    private String name;
    private String catchPhrase;
    private String bs;
}
