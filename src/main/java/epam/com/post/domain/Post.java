package epam.com.post.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Post {
    private long userId;
    private long id;
    private String title;
    private String body;
}
