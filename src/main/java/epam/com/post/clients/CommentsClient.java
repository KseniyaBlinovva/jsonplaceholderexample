package epam.com.post.clients;

import epam.com.post.domain.Comment;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "${feign.commentsName}", url = "${feign.url}")
public interface CommentsClient {
    @GetMapping("/comments")
    List<Comment> getComments(@RequestParam("postId") long postId);
}
