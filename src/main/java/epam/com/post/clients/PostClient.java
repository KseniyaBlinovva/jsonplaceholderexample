package epam.com.post.clients;

import epam.com.post.domain.Post;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "${feign.postName}", url = "${feign.url}", decode404 = true)
public interface PostClient {
    @GetMapping("/posts")
    List<Post> getUserPost(@RequestParam("userId") long userId,@RequestParam("count") int count);

}
