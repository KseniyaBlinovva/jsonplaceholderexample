package epam.com.post.service;

import epam.com.post.clients.CommentsClient;
import epam.com.post.clients.PostClient;
import epam.com.post.domain.Comment;
import epam.com.post.domain.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class FeignService {
    @Autowired
    PostClient postClient;
    @Autowired
    CommentsClient commentsClient;

    public Map<Integer, List<Comment>> getUserPost(long userId, int count) {
        return (postClient.getUserPost(userId,count)
                .stream()
                .mapToLong(Post::getId)
                .mapToObj(postId -> commentsClient.getComments(postId))
                .map(comments -> comments.subList(comments.size() - count, comments.size()))
                .collect(Collectors.toMap(list -> list.get(0).getId(), list -> list)));

    }

}
