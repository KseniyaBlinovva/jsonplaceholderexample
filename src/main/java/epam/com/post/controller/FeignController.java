package epam.com.post.controller;

import epam.com.post.domain.Comment;
import epam.com.post.service.FeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class FeignController {
    @Autowired
    FeignService feignService;

    @GetMapping("/posts")
    public Map<Integer, List<Comment>> getUserPost(@RequestParam long userId, @RequestParam(defaultValue = "3") int count) {
        return feignService.getUserPost(userId,count);
    }

}
